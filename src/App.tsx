import * as React from 'react';
import { ServiceAgent, ICallResponse } from 'grey-service-agent';
import './App.css';

class App extends React.Component {
    public render() {
        return (
            <div onClick={async () => {
                const p = await serviceAgent.get('timg.jpeg').responseType('blob').end(this);
                if (!p.err) {
                    const a = document.createElement("a");
                    a.href = window.URL.createObjectURL(p.res.body);
                    a.download = 'timg.jpg';
                    a.click()
                }
            }} className="App">
                123
            </div>
        );
    }
}

const url = '/auth/web/login?333=213';

function isok(e: ICallResponse) {
    console.log('ICallResponse:', e);
    if (e.err) {
        return;
    }
    return e.res;
}

const serviceAgent = new ServiceAgent();

serviceAgent.domain = 'http://localhost:3000/';// 配置网关

// 使用方法一
serviceAgent.call(this, 'get', url, [isok, (e: any) => {
    // console.log(e);
}]);

// serviceAgent.send({
//     target: this,
//     sar: superagent.post('http://wwww.baidu.com/s').set({}),
//     fus: [isok, (e: any) => {
//         // console.log(e);
//     }],
// });

// 使用方法二
serviceAgent.create('get', url, {}).end([isok, (e: any) => {
    // console.log(e);
}]);

// 使用方法三
// serviceAgent.create('get', url, {}, 'http://wwww.baidu.com').set({}).end(this, [
//     isok,
//     (e: any) => {
//         console.log(e);
//     }
// ]);

// 使用方法四 (省约第一参数)
// serviceAgent.get(url, {}).end(this);
// serviceAgent.post(url, {}).end(this);
// serviceAgent.del(url, {}).end(this);
// serviceAgent.put(url, {}).end(this);

// 使用方法五
(async () => {
    const params: any = {}
    let cres = await serviceAgent.create('get', url, params).end(this);
    if (cres.err) {
        return;
    }

    cres = await serviceAgent.call(this, 'get', url, params);
    if (cres.err) {
        return;
    }
})();

export default App;
